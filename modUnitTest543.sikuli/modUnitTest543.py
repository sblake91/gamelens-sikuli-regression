from sikuli import *
import shutil
import os
import util
import unittest
gamelens = util.gamelens

class UnitTest543(unittest.TestCase):
    def test00setup(self):
        util.setUp()
        
    def testTearDown(self):
        util.tearDown()

    def test01(self):
        if exists("basic_menu.png"):
             click(Pattern("basic_menu.png").targetOffset(-321,-274))
             wait(2)
        click("new_game_button.png")
        wait(2)
        click(Pattern("new_game_screen.png").targetOffset(-470,-324))
        type("12345678")
        click(Pattern("new_game_screen.png").targetOffset(-337,-267))
        type("Unique Competition")
        click(Pattern("new_game_screen.png").targetOffset(-325,-182))
        type("Test Home")
        click(Pattern("new_game_screen.png").targetOffset(-79,-285))
        type("Unique")
        click(Pattern("new_game_screen.png").targetOffset(145,-287))
        type("Player")
        click(Pattern("new_game_screen.png").targetOffset(577,313))
        wait("autofill_prompt.png")
        click(Pattern("autofill_prompt.png").targetOffset(-50,29))
        wait("tasks_screen.png")
        click(Pattern("tasks_screen.png").targetOffset(433,-286))
        wait("template_select.png")
        click(Pattern("template_select.png").targetOffset(-210,-111))
        click(Pattern("template_select.png").targetOffset(135,131))
        click(Pattern("tasks_screen2.png").targetOffset(436,326))
        wait("game_created.png",3)
        click(Pattern("game_created.png").targetOffset(-4,26))
        wait("database_management_buttons.png")
        click(Pattern("database_management_buttons.png").targetOffset(-218,-2))
        wait("player_management.png",15)
        click(Pattern("player_management.png").targetOffset(436,-259))
        type("Player")
        click(Pattern("player_management.png").targetOffset(472,68))
        wait("test_player.png")
        click(Pattern("test_player.png").targetOffset(235,0))           
        wait("replace_player.png")
        click(Pattern("replace_player.png").targetOffset(-412,-200))
        click(Pattern("replace_player.png").targetOffset(-346,36))
        type("Home Team")
        click(Pattern("replace_player.png").targetOffset(-292,37))
        type("1")
        click("home_team1.png")
        click("game_to_select.png")
        click("transfer_button.png")
        wait(1)
        click(Pattern("player_to_delete.png").targetOffset(210,-2))
        click(Pattern("1513282945347.png").targetOffset(-47,30))
        wait(1)
        assert not exists(Pattern("player_to_delete.png").targetOffset(210,-2))

    def test02(self):
        click(Pattern("red_menu_bar.png").targetOffset(-22,1))
        wait("game_management.png", 15)
        find(Pattern("game_to_delete.png").similar(0.90))        
        click(Pattern("game_to_delete.png").similar(0.90).targetOffset(236,-2))        
        click("confirm_delete.png")
        click(Pattern("confirm_delete.png").targetOffset(-48,32))
        wait(1)
        assert not exists(Pattern("game_to_delete.png").similar(0.87).targetOffset(236,-2))

    def test03(self):
        click("home.png")
        wait(1)
        click("advanced_management.png")
        wait("advanced_management_screen.png")
        click(Pattern("advanced_management_screen.png").targetOffset(-190,-240))
        wait("export_successs.png", 15)
        assert exists("export_successs.png")
        click(Pattern("export_successs.png").targetOffset(-5,31))