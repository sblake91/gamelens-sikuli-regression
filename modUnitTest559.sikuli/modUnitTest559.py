from sikuli import *
import shutil
import os
import util
import unittest
gamelens = util.gamelens
#Settings.MoveMouseDelay = 0

class UnitTest559(unittest.TestCase):
    def test00setup(self):
        util.setUp()
      
    def testTearDown(self):
        util.tearDown()
        
    def test01(self):
        wait(1)
        click(Pattern("1513375648605.png").targetOffset(-26,224))
        wait("preferences_buttons.png")
        click(Pattern("preferences_buttons.png").targetOffset(2,-56))
        wait("keyboard_shortcuts.png")
        click(Pattern("keyboard_shortcuts.png").targetOffset(-180,200))
        findAll("1513372365945.png")
        mm = SCREEN.getLastMatches()
        while mm.hasNext():
            click(mm.next())
            type("A")
        click("save.png")
        wait("duplicate_keys_error.png", 5)
        assert exists("duplicate_keys_error.png")
        
        
    def test02(self):
        click(Pattern("duplicate_keys_error.png").targetOffset(145,234))
        wait(1)
        assert exists(Pattern("all_as.png").exact())

        
    def test03(self):
        click(Pattern("keyboard_shortcuts.png").targetOffset(-181,223))
        assert exists(Pattern("keyboard_shortcuts.png").exact())
