from sikuli import *
import shutil
import os
import util
import unittest

class UnitTest4(unittest.TestCase):
    
    def test00setup(self):
        util.setUp()
        
    def testTearDown(self):
        if not exists(Pattern("drawing_in_forum.png").similar(0.80)):
                click(Pattern("1518710428045.png").similar(0.90).targetOffset(-80,0))
                
        rightClick(Pattern("drawing_in_forum.png").similar(0.80))
        wait("1514403759390.png")
        click(Pattern("1514403759390.png").targetOffset(-39,26))
        wait("1514403806009.png")
        click(Pattern("1514403806009.png").targetOffset(-50,32))
        util.tearDown()
        
    def test01(self):
        click("manage_vsession.png")
        wait("vession_manager.png")
        if not exists(Pattern("test_vsession_expanded.png").similar(0.90)):
                click(Pattern("expand_test_vsession.png").similar(0.90).targetOffset(-38,1))
        rightClick(Pattern("expand_test_vsession.png").similar(0.90).targetOffset(3,2))
        wait("vsession_right_click1.png")
        click(Pattern("vsession_right_click1.png").targetOffset(-47,-109))
        click(Pattern("vession_manager.png").targetOffset(-455,258))
        click(Pattern("vession_manager.png").targetOffset(-275,383))
        wait("drawing_tools.png")
        click(Pattern("drawing_tools.png").targetOffset(-97,15))
        wait("drawing_font_size.png")
        click(Pattern("drawing_font_size.png").targetOffset(-6,30))
        click(Pattern("drawing_tools.png").targetOffset(-114,29))
        click(Pattern("vession_manager.png").targetOffset(-300,58))
        type("DRAWING")
        click(Pattern("drawing_tools.png").targetOffset(60,26))
        assert exists("drawing_indicator.png") and exists("drawing_under_tag.png")

    def test02(self):
        doubleClick("drawing_under_tag.png")
        wait(5)
        assert exists("drawing.png")

    def test03(self):
        click(Pattern("vession_manager.png").targetOffset(-455,258))
        wait(1)
        click(Pattern("vession_manager.png").targetOffset(-455,258))
        assert not exists("drawing.png")

    def test04(self):
        doubleClick(Pattern("expand_test_vsession.png").similar(0.90).targetOffset(3,2))
        wait(5)
        assert exists("drawing.png")

    def test05(self):
        dragDrop(Pattern("vession_manager.png").targetOffset(213,204), Pattern("vession_manager.png").targetOffset(78,30))
        assert exists("drawing_scaled.png")
        

        