from sikuli import *
import shutil
import os
import util
import unittest

class UnitTest1180(unittest.TestCase):
    
    def test00setup(self):
        util.setUp()
        
    def testTearDown(self):
        rightClick("drawing_in_forum.png")
        wait("drawing_right_click.png")
        click(Pattern("drawing_right_click.png").targetOffset(-39,26))
        wait("confirm_delete_drawing.png")
        click(Pattern("confirm_delete_drawing.png").targetOffset(-50,32))
        wait(2)
        util.tearDown()
        
    def test01(self):
        click("manage_vsession.png")
        wait("vession_manager.png")
        if not exists(Pattern("test_vsession_expanded.png").similar(0.90)):
                click(Pattern("expand_test_vsession.png").similar(0.90).targetOffset(-38,1))
        rightClick(Pattern("expand_test_vsession.png").similar(0.90).targetOffset(3,2))
        wait("vsession_right_click1.png")
        click(Pattern("vsession_right_click1.png").targetOffset(-47,-109))
        click(Pattern("vession_manager.png").targetOffset(-455,258))
        click(Pattern("vession_manager.png").targetOffset(-275,383))
        wait("drawing_tools.png")
        click(Pattern("drawing_tools.png").targetOffset(-97,15))
        wait("drawing_font_size.png")
        click(Pattern("drawing_font_size.png").targetOffset(-6,30))
        click(Pattern("drawing_tools.png").targetOffset(-114,29))
        click(Pattern("vession_manager.png").targetOffset(-300,58))
        type("DRAWING")
        click(Pattern("drawing_tools.png").targetOffset(60,26))
        if not exists(Pattern("drawing_in_forum.png").similar(0.80)):
            click(Pattern("tag_collapsed.png").similar(0.90).targetOffset(-74,-1))
            
        assert exists("drawing_indicator.png") and exists("drawing_under_tag.png")

    def test02(self):
        rightClick(Pattern("drawing_under_tag-2.png").targetOffset(-103,20))
        wait("drawing_right_click_menu.png")
        click(Pattern("drawing_right_click_menu.png").targetOffset(-29,51))
        assert exists("drawing-1.png")

    def test03(self):
        click(Pattern("drawing_tools.png").targetOffset(108,-16))
        wait(1)
        assert exists("drawing-1.png")

    def test04(self):
        if not exists("drawing-1.png"):
            click("drawing_in_forum.png")
            rightClick("drawing_in_forum.png")
            wait("drawing_right_click.png")
            click(Pattern("drawing_right_click.png").targetOffset(-30,51))
            click(Pattern("drawing_tools.png").targetOffset(-97,15))
            wait("drawing_font_size.png")
            click(Pattern("drawing_font_size.png").targetOffset(-6,30))
            click(Pattern("drawing_tools.png").targetOffset(-114,29))
            click(Pattern("vession_manager.png").targetOffset(-300,58))
        click(Pattern("vession_manager.png").targetOffset(-352,-314))
        type("EDITED")
        assert exists("drawing-1.png") and exists("edited_drawing.png")
        
    def test05(self):
        click(Pattern("drawing_tools.png").targetOffset(108,-16))
        wait(1)
        assert not exists("edited_drawing.png") and exists("drawing-1.png")

    def test06(self):
        click(Pattern("vession_manager.png").targetOffset(-352,-314))
        type("EDITED")
        click(Pattern("drawing_tools.png").targetOffset(63,23))
        doubleClick("drawing_in_forum.png")
        assert exists("drawing-1.png") and exists("edited_drawing.png")
        