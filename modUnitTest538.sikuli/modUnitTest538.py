from sikuli import *
import shutil
import os
import util
import unittest
gamelens = util.gamelens
backgroundPath =  util.backgroundPath
logoPath =  util.logoPath

class UnitTest538(unittest.TestCase):
    def test00setup(self):
        util.setUp()
        
    def testTearDown(self):
        util.tearDown()
        
    def test01a(self):
        click(Pattern("toggle_menu.png").targetOffset(-22,-32))
        
        wait(1)
        assert exists(Pattern("basic_home.png").similar(0.90))

    def test01b(self):
        click("review_a_game.png")
        wait(1)
        assert exists(Pattern("Competition_List.png").similar(0.90))
        
    def test02(self):
        click("cancel.png")
        wait(1)
        click(Pattern("advanced_home.png").targetOffset(-575,231))
        wait("preferences.png")
        click(Pattern("preferences.png").targetOffset(-275,-112))
        wait("personalization.png")
        assert exists(Pattern("personalization.png").targetOffset(72,-162))

    def test02a(self):
        click(Pattern("personalization.png").targetOffset(72,-162))
        #wait(Pattern("file_browse.png").similar(0.50))
        #click(Pattern("file_browse.png").similar(0.50).targetOffset(-280,197))
        wait(1)
        type(backgroundPath + Key.ENTER)
        wait("personalization.png")
        click(Pattern("personalization.png").targetOffset(135,-85))
        wait(1)
        #wait(Pattern("file_browse.png").similar(0.22))
        type(logoPath + Key.ENTER)
        wait(Pattern("personalization.png").targetOffset(-128,-47))
        click(Pattern("personalization.png").targetOffset(-126,-49))
        click(Pattern("personalization.png").targetOffset(195,-47))
        type("1/1/2030")
        click(Pattern("personalization.png").targetOffset(-131,-9))
        click(Pattern("personalization.png").targetOffset(174,140))
        click(Pattern("personalization.png").targetOffset(88,95))
        click(Pattern("personalization.png").targetOffset(-89,64))
        click(Pattern("personalization.png").targetOffset(98,69))

    def test03(self):
        click(Pattern("personalization.png").targetOffset(173,267))
        wait(1)
        assert exists("customized home.png")
        
    def test03a(self):
        click("review_a_game.png")
        wait(1)
        assert not exists(Pattern("Competition_List.png").similar(0.90))

    def test03b(self):
        click("cancel.png")
        click(Pattern("main_sidebar.png").targetOffset(-31,222))
        wait("preferences.png")
        click(Pattern("preferences.png").targetOffset(-275,-112))
        wait("personalization.png")
        click(Pattern("personalization.png").targetOffset(155,-160))
        click(Pattern("personalization.png").targetOffset(203,-88))
        click(Pattern("personalization.png").targetOffset(-131,-50))
        click(Pattern("personalization.png").targetOffset(274,-49))
        for i in range(0,10):
            type(Key.BACKSPACE)
        click(Pattern("personalization.png").targetOffset(-129,-9))
        click(Pattern("personalization.png").targetOffset(189,139))
        click(Pattern("personalization.png").targetOffset(90,103))
        click(Pattern("personalization.png").targetOffset(-93,120))
        click(Pattern("personalization.png").targetOffset(96,68))
        click(Pattern("personalization.png").targetOffset(181,268))
        wait(1)
        assert exists(Pattern("advanced_home.png").targetOffset(-588,-315))
