from sikuli import *
import shutil
import os
import util
import unittest

class UnitTest1181(unittest.TestCase):
    
    def test00setup(self):
        util.setUp()
        
    def testTearDown(self):
        rightClick("drawing_in_forum.png")
        wait("drawing_right_click.png")
        click(Pattern("drawing_right_click.png").targetOffset(-39,26))
        wait("confirm_delete_drawing.png")
        click(Pattern("confirm_delete_drawing.png").targetOffset(-50,32))
        wait(2)
        util.tearDown()

    def test01(self):
        click("manage_vsession.png")
        wait("vession_manager.png")
        if not exists(Pattern("test_vsession_expanded.png").similar(0.90)):
                click(Pattern("expand_test_vsession.png").similar(0.90).targetOffset(-38,1))
        rightClick(Pattern("expand_test_vsession.png").similar(0.90).targetOffset(3,2))
        wait("vsession_right_click1.png")
        click(Pattern("vsession_right_click1.png").targetOffset(-47,-109))
        click(Pattern("vession_manager.png").targetOffset(-455,258))
        click(Pattern("vession_manager.png").targetOffset(-275,383))
        wait("drawing_tools.png")
        click(Pattern("drawing_tools.png").targetOffset(-97,15))
        wait("drawing_font_size.png")
        click(Pattern("drawing_font_size.png").targetOffset(-6,30))
        click(Pattern("drawing_tools.png").targetOffset(-114,29))
        click(Pattern("vession_manager.png").targetOffset(-300,58))
        type("DRAWING")
        click(Pattern("drawing_tools.png").targetOffset(60,26))
        if not exists("drawing_under_tag-1.png"):
            click(Pattern("forst_test_tag.png").targetOffset(-71,1))
        assert exists("drawing_indicator.png") and exists("drawing_under_tag.png")

    def test02(self):
        rightClick("drawing_in_forum.png")
        wait("drawing_right_click.png")
        click(Pattern("drawing_right_click.png").targetOffset(-48,0))
        wait("edit_drawing_menu.png")
        click(Pattern("edit_drawing_menu.png").targetOffset(97,-44))
        type(Key.BACKSPACE + "0")
        click(Pattern("edit_drawing_menu.png").targetOffset(92,110))
        doubleClick("drawing_in_forum.png")
        wait(2.5)
        assert exists("drawing-1.png")

    def test03(self):
        rightClick("drawing_in_forum.png")
        wait("drawing_right_click.png")
        click(Pattern("drawing_right_click.png").targetOffset(-48,0))
        wait("edit_drawing_menu.png")
        click(Pattern("edit_drawing_menu.png").targetOffset(97,-44))
        type(Key.BACKSPACE + "2")
        click(Pattern("edit_drawing_menu.png").targetOffset(92,110))
        doubleClick("drawing_in_forum.png")
        wait(2.5)
        assert not exists("drawing-1.png")
    