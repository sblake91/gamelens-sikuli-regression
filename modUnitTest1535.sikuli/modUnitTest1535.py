from sikuli import *
import shutil
import os
import util
import unittest

class UnitTest1535(unittest.TestCase):
    
    def test00setup(self):
        util.setUp()
        
    def testTearDown(self):
        util.tearDown()

    def test01(self):
        click("manage_vsession.png")
        if exists("test_vsession_expanded.png"):
            click(Pattern("vsession_test_collapse.png").targetOffset(-40,2))
        if exists("folder_expanded.png"):
            click(Pattern("folder_expanded.png").targetOffset(-182,-30))
        elif not exists("away_test_expanded.png"):
            click(Pattern("away_test_expand.png").similar(0.80).targetOffset(-49,-2))
        dragDrop(Pattern("away_test_expanded.png").targetOffset(-109,29), "vsession_test_collapse.png")

        assert exists("test_vsession_drag_n_drop.png")
        

    def test02(self):
        dragDrop(Pattern("test_vsession_drag_n_drop.png").targetOffset(-117,-28), Pattern("away_test_expanded.png").similar(0.50).targetOffset(-147,-12))
        assert exists(Pattern("folder_drag_n_drop.png").similar(0.90))

    def test03(self):
        dragDrop(Pattern("folder_drag_n_drop.png").similar(0.90).targetOffset(-96,-13), Pattern("folder_drag_n_drop.png").similar(0.90).targetOffset(-105,58))
        assert exists("folder_expanded.png")
    
        
        
            
        