from sikuli import *

import unittest
import shutil
import util
import os
import unittest
import HTMLTestRunner
reload(HTMLTestRunner)

from modUnitTest2 import *
from modUnitTest4 import *
from modUnitTest5 import *
from modUnitTest537 import *
from modUnitTest538 import *
from modUnitTest539 import *
from modUnitTest540 import *
from modUnitTest541 import *
from modUnitTest543 import *
from modUnitTest545 import *
from modUnitTest549 import *
from modUnitTest554 import *
from modUnitTest556 import *
from modUnitTest558 import *
from modUnitTest559 import *
from modUnitTest575 import *
from modUnitTest1180 import *
from modUnitTest1181 import *
from modUnitTest1183 import *
from modUnitTest1184 import *
from modUnitTest1308 import *
from modUnitTest1319 import *
from modUnitTest1532 import *
from modUnitTest1535 import *
from modUnitTest1886 import *
from modUnitTest1889 import *
from modUnitTest1891 import *
from modUnitTest1898 import *
from modUnitTest1899 import *

#suite=unittest.TestLoader().loadTestsFromTestCase(UnitTest538)

suite=unittest.TestLoader().loadTestsFromTestCase(UnitTest537)
suite.addTests(unittest.TestLoader().loadTestsFromTestCase(UnitTest540))
suite.addTests(unittest.TestLoader().loadTestsFromTestCase(UnitTest538))
suite.addTests(unittest.TestLoader().loadTestsFromTestCase(UnitTest539))
suite.addTests(unittest.TestLoader().loadTestsFromTestCase(UnitTest540))
suite.addTests(unittest.TestLoader().loadTestsFromTestCase(UnitTest541))
suite.addTests(unittest.TestLoader().loadTestsFromTestCase(UnitTest543))
suite.addTests(unittest.TestLoader().loadTestsFromTestCase(UnitTest545))
suite.addTests(unittest.TestLoader().loadTestsFromTestCase(UnitTest549))
suite.addTests(unittest.TestLoader().loadTestsFromTestCase(UnitTest554))
suite.addTests(unittest.TestLoader().loadTestsFromTestCase(UnitTest556))
suite.addTests(unittest.TestLoader().loadTestsFromTestCase(UnitTest558))
suite.addTests(unittest.TestLoader().loadTestsFromTestCase(UnitTest559))
suite.addTests(unittest.TestLoader().loadTestsFromTestCase(UnitTest575))
suite.addTests(unittest.TestLoader().loadTestsFromTestCase(UnitTest4))
suite.addTests(unittest.TestLoader().loadTestsFromTestCase(UnitTest5))
suite.addTests(unittest.TestLoader().loadTestsFromTestCase(UnitTest1180))
suite.addTests(unittest.TestLoader().loadTestsFromTestCase(UnitTest1181))
suite.addTests(unittest.TestLoader().loadTestsFromTestCase(UnitTest1183))
suite.addTests(unittest.TestLoader().loadTestsFromTestCase(UnitTest1184))
suite.addTests(unittest.TestLoader().loadTestsFromTestCase(UnitTest1308))
suite.addTests(unittest.TestLoader().loadTestsFromTestCase(UnitTest1532))
suite.addTests(unittest.TestLoader().loadTestsFromTestCase(UnitTest1535))
suite.addTests(unittest.TestLoader().loadTestsFromTestCase(UnitTest1319))
suite.addTests(unittest.TestLoader().loadTestsFromTestCase(UnitTest1886))
suite.addTests(unittest.TestLoader().loadTestsFromTestCase(UnitTest1889))
suite.addTests(unittest.TestLoader().loadTestsFromTestCase(UnitTest1891))
suite.addTests(unittest.TestLoader().loadTestsFromTestCase(UnitTest1898))
suite.addTests(unittest.TestLoader().loadTestsFromTestCase(UnitTest1899))


Settings.MoveMouseDelay = 0.5

outfile = open(util.folderPath + "\Report.html", "w")
#unittest.TextTestRunner(verbosity=2).run(suite)
runner = HTMLTestRunner.HTMLTestRunner(stream=outfile, title='GameLens Regression', description='GameLens Regression Suite', dirTestScreenshots = util.screenshotsFolder )
runner.run(suite)