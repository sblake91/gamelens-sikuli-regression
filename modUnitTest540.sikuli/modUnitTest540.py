from sikuli import *
import shutil
import os
import util
import unittest
gamelens = util.gamelens
GLXPath = util.GLXPath

class UnitTest540(unittest.TestCase):
    def test00setup(self):
        util.setUp()
        
    def testTearDown(self):
        util.tearDown()
        
    def test01(self):
        click(Pattern("advanced_home.png").targetOffset(-589,175))
        wait("browse_files.png", 5)        
        click(Pattern("browse_files.png").targetOffset(306,14))
        wait(1)
        type(GLXPath + Key.ENTER)
        find(Pattern("replace_or_append_prompt.png").similar(0.93))
        click(Pattern("replace_or_append_prompt.png").similar(0.85).targetOffset(-8,42))
        wait("import_complete.png")
        assert exists("import_complete.png")

    def test02(self):
        click(Pattern("import_complete.png").targetOffset(-4,22))
        click(Pattern("advanced_home.png").targetOffset(-577,-270))
        click(Pattern("advanced_home.png").targetOffset(-29,-83))
        wait("game_select.png")
        click(Pattern("game_select.png").targetOffset(-590,-118))
        click(Pattern("game_select.png").targetOffset(299,-286))
        click(Pattern("game_select.png").targetOffset(555,325))
        wait("game_details.png")
        assert exists(Pattern("game_details.png").similar(0.90))
