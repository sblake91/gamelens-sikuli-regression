from sikuli import *
import shutil
import os
import util
import unittest

class UnitTest2(unittest.TestCase):
    
    def test00setup(self):
        if os.path.exists(r"C:\STATS\GameLensRU\Edited Clips\DELETE_ME.mp4"):
            os.remove(r"C:\STATS\GameLensRU\Edited Clips\DELETE_ME.mp4")
        util.setUp()
       
    def testTearDown(self):
        if os.path.exists(r"C:\STATS\GameLensRU\Edited Clips\DELETE_ME.mp4"):
            os.remove(r"C:\STATS\GameLensRU\Edited Clips\DELETE_ME.mp4")
        util.tearDown()

    def test01(self):
        click("manage_vsession.png")
        wait("vsession_viewer.png")
        if not exists(Pattern("test_vsession_expanded.png").similar(0.90)):
            click(Pattern("expand_test_vsession.png").similar(0.90).targetOffset(-38,1))
        doubleClick( Pattern("test_vsession_expanded.png").similar(0.90).targetOffset(-83,-18))
        click(Pattern("vsession_viewer.png").similar(0.85).targetOffset(-459,267))
        click(Pattern("vsession_viewer.png").similar(0.85).targetOffset(-280,333))
        wait(Pattern("rename_video_clip_to.png").similar(0.90))
        wait(2)
        paste("DELETE_ME")
        click(Pattern("rename_video_clip_to.png").similar(0.90).targetOffset(61,42))
        click(Pattern("vsession_viewer.png").similar(0.85).targetOffset(394,-371))
        wait(1)
        paste(r"C:\STATS\GameLensRU\Edited Clips\DELETE_ME.mp4")
        click("open_button.png")
        assert exists(Pattern("clip_added_to_vsession.png").similar(0.90))

    def test02(self):
        click(Pattern("vession_manager.png").targetOffset(-275,383))
        wait("drawing_tools.png")
        click(Pattern("drawing_tools.png").targetOffset(-97,15))
        wait("drawing_font_size.png")
        click(Pattern("drawing_font_size.png").targetOffset(-6,30))
        click(Pattern("drawing_tools.png").targetOffset(-114,29))
        click(Pattern("vession_manager.png").targetOffset(-300,58))
        type("DRAWING")
        click(Pattern("drawing_tools.png").targetOffset(60,26))
        if not exists("drawing_under_tag.png"):
            click(Pattern("first_test_tag.png").targetOffset(-71,1))
            
        assert exists("drawing_indicator.png") and exists("drawing_under_tag.png")

    def test03(self):
        click(Pattern("vsession_viewer.png").similar(0.85).targetOffset(-459,267))
        wait(2)
        click(Pattern("vsession_viewer.png").similar(0.85).targetOffset(-459,267))
        click(Pattern("drawing_tools.png").targetOffset(-114,29))
        click(Pattern("vession_manager.png").targetOffset(-386,-297))
        type("DRAWING2")
        click(Pattern("drawing_tools.png").targetOffset(60,26))
        drawingCount = util.returnImageCount("drawing_indicator.png")
        assert drawingCount == 2 and exists("two_drawings_under_tag.png")

    def test04(self):
        doubleClick("delete_me_clip.png")
        click(Pattern("vsession_viewer.png").similar(0.85).targetOffset(-459,267))
        click(Pattern("drawing_tools.png").targetOffset(-114,29))
        click(Pattern("vession_manager.png").targetOffset(-386,-297))
        type("DRAWING3")
        click(Pattern("drawing_tools.png").targetOffset(60,26))
        if not exists("clip_expanded_with_drawing.png"):
            click(Pattern("clip_collapsed.png").similar(0.90).targetOffset(-40,0))
        assert exists("clip_expanded_with_drawing.png") and exists("drawing_indicator.png")

    def test05(self):
        click(Pattern("vsession_viewer.png").similar(0.85).targetOffset(-459,267))
        wait(2)
        click(Pattern("vsession_viewer.png").similar(0.85).targetOffset(-459,267))
        click(Pattern("drawing_tools.png").targetOffset(-114,29))
        click(Pattern("vession_manager.png").targetOffset(-386,-297))
        type("DRAWING4")
        click(Pattern("drawing_tools.png").targetOffset(60,26))
        wait(2)
        drawingCount2 = util.returnImageCount("drawing_indicator.png")
        assert drawingCount2 == 2 and exists(Pattern("two_drawings_under_clip.png").similar(0.90))

    def test06(self):
        rightClick(Pattern("two_drawings_under_tag.png").targetOffset(-90,10))
        wait("drawing_right_click.png")
        click(Pattern("drawing_right_click.png").targetOffset(-44,0))
        wait("edit_drawing_settings.png")
        click(Pattern("edit_drawing_settings.png").targetOffset(104,-77))
        type(Key.BACKSPACE + "1")
        click(Pattern("edit_drawing_settings.png").targetOffset(91,112))
        doubleClick(Pattern("two_drawings_under_tag.png").targetOffset(-90,10))
        click(Pattern("vsession_viewer.png").similar(0.85).targetOffset(-459,267))
        wait(1.5)
        click(Pattern("vsession_viewer.png").similar(0.85).targetOffset(-459,267))
        assert not exists("first_drawing.png")

    def test07(self):
        rightClick(Pattern("clip_collapsed.png").targetOffset(14,0))
        wait("right_click_clip.png")
        click(Pattern("right_click_clip.png").targetOffset(-44,20))
        wait("confirm_delete_clip.png")
        click(Pattern("confirm_delete_clip.png").targetOffset(-51,26))
        for i in (0, 2):
            rightClick(Pattern("drawing_in_menu.png").similar(0.90).targetOffset(-1,-3))
            wait("drawing_right_click.png")
            click(Pattern("drawing_right_click.png").targetOffset(-45,25))
            wait("confirm_delete_clip.png")
            click(Pattern("confirm_delete_clip.png").targetOffset(-51,26))
            wait(1)
        assert not exists(Pattern("drawing_in_menu.png").similar(0.90)) and not exists(Pattern("delete_me_clip.png").similar(0.90))
        