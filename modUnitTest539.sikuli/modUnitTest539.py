from sikuli import *
import shutil
import os
import util
import unittest
gamelens = util.gamelens

class UnitTest539(unittest.TestCase):
    def test00setup(self):
        util.setUp()
        
    def testTearDown(self):
        util.tearDown()

    def test01(self):
        click("review_game_button.png")
        wait("game_selection.png")
        assert exists("game_selection.png")
        
    def test02(self):
        click(Pattern("game_selection.png").targetOffset(-594,-121))
        click(Pattern("game_selection.png").targetOffset(406,-277))
        click(Pattern("game_selection.png").targetOffset(546,325))
        wait("game_summary.png")
        assert exists("game_summary.png")
        
    def test03(self):
        click(Pattern("game_summary.png").targetOffset(-579,-228))
        wait(2)
        assert exists(Pattern("player_videos.png").similar(0.90))

    def test03a(self):
        click(Pattern("team_videos4.png").targetOffset(306,-154))
        click(Pattern("game_summary.png").targetOffset(-381,-347))
        wait(2)
        assert exists("field.png")

    def test03b(self):
        click(Pattern("team_filtering.png").targetOffset(-121,10))
        click(Pattern("filter_buttons.png").targetOffset(-59,-1))
        wait(2)
        assert not exists(Pattern("blue_dots.png").similar(0.90))
        
    def test03c(self):
        click(Pattern("filter_buttons.png").targetOffset(63,2))
        
        click(Pattern("menu_bar.png").targetOffset(36,17))
        wait(3)
        assert exists(Pattern("player_summary.png").similar(0.90))

    def test03d(self):
        click(Pattern("player_summary.png").similar(0.90).targetOffset(54,46))
        wait(2)
        assert exists("team_videos3.png")
        
    def test03e(self):
        click(Pattern("team_videos4.png").targetOffset(306,-154))
        click(Pattern("menu_bar.png").targetOffset(157,19))
        wait(3)
        assert exists(Pattern("team_summary.png").similar(0.90).targetOffset(-96,50))

    def test03f(self):
        click(Pattern("team_summary.png").similar(0.90).targetOffset(-96,50))
        wait(2)
        assert exists(Pattern("team_videos2.png").similar(0.90))   

    def test03g(self):
        click(Pattern("team_videos4.png").targetOffset(306,-154))
        click(Pattern("menu_bar.png").targetOffset(271,17))
        click(Pattern("favorites.png").targetOffset(-33,24))
        
        wait(1)
        assert exists(Pattern("report.png").similar(0.90))   
        
