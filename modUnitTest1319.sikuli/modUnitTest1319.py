from sikuli import *
import shutil
import os
import util
import unittest
gamelens = util.gamelens

class UnitTest1319(unittest.TestCase):
    def test00setup(self):
        util.setUp()
        
    def testTearDown(self):
        util.tearDown()

    def test01(self):
        click("manage_vsession.png")
        wait("vsession_view.png")
        rightClick("test_vsession.png")
        assert exists("right_click_vsession.png")
        
    def test02(self):
        click(Pattern("right_click_vsession.png").targetOffset(-42,-57))
        wait("success_popup.png", 120)
        assert exists(Pattern("success_popup2.png").targetOffset(-5,28))
        click(Pattern("success_popup2.png").targetOffset(-5,28))