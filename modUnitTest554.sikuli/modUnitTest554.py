from sikuli import *
import shutil
import os
import util
import unittest

class UnitTest554(unittest.TestCase):
    
    def test00setup(self):
        util.setUp()
        
    def testTearDown(self):
        util.tearDown()

    def test01(self):
        click("task_filter_engine_button.png")
        wait("task_filter_screen.png")
        dragDrop("att_ruck.png", "favourite_1_folder.png")
        assert exists(Pattern("task_added_to_folder.png").similar(0.95))

    def test02(self):
        click(Pattern("task_added_to_folder.png").similar(0.95).targetOffset(-32,25))
        click(Pattern("task_filter_screen.png").targetOffset(-362,327))
        wait("confirm_delete_task.png")
        click(Pattern("confirm_delete_task.png").targetOffset(-53,29))
        assert not exists(Pattern("task_added_to_folder.png").similar(0.95))

    def test03(self):
        click("att_ruck.png")
        click(Pattern("task_filter_screen.png").targetOffset(438,340))
        wait("game_load_screen.png")
        click(Pattern("game_load_screen.png").targetOffset(374,362))
        assert exists("advanced_filters.png")
        