from sikuli import *
import shutil
import os
import util
import unittest

class UnitTest5(unittest.TestCase):
    
    def test00setup(self):
        util.setUp()
        
    def testTearDown(self):
        util.tearDown()

    def test01(self):
        click("manage_vsession.png")
        wait("vession_manager.png")
        click(Pattern("vession_manager.png").targetOffset(463,21))
        wait(1)
        if not exists(Pattern("test_vsession_expanded.png").similar(0.90)):
                click(Pattern("expand_test_vsession.png").similar(0.90).targetOffset(-38,1))
                
        rightClick(Pattern("expand_test_vsession.png").similar(0.90).targetOffset(7,-2))
        wait("vsession_right_click.png")
        click(Pattern("vsession_right_click.png").targetOffset(-47,-109))
        click(Pattern("vession_manager.png").targetOffset(429,126))
        wait(2)
        assert exists(Pattern("line_indicating_current_clip.png").similar(0.80)) and exists(Pattern("clip_one_title_in_viewer.png").similar(0.80)) and exists(Pattern("first_clip_red_indicator.png").similar(0.90))
        

    def test02(self):
        click(Pattern("previous_and_next_buttons.png").targetOffset(34,-4))
        wait(1)
        assert exists(Pattern("clip_two_title_in_viewer.png").similar(0.80)) and exists(Pattern("clip_number_2_of_4.png").similar(0.80)) and exists("second_video_red_indicator.png")
    def test03(self):
        click(Pattern("previous_and_next_buttons.png").similar(0.50).targetOffset(-29,-4))
        wait(1)
        assert exists(Pattern("clip_1_of_4.png").similar(0.80)) and exists(Pattern("clip_one_title_in_viewer.png").similar(0.80)) and exists(Pattern("first_clip_red_indicator.png").similar(0.80))
        
     
      
       
       
            
        