from sikuli import *
import shutil
import os
import util
import unittest
gamelens = util.gamelens

class UnitTest1886(unittest.TestCase):
    def test00setup(self):
        util.setUp()
        
    def testTearDown(self):
        util.tearDown()

    def test01(self):
        click(Pattern("review_a_game.png").similar(0.80),2)
        wait("game_selection.png")
        click(Pattern("game_selection.png").targetOffset(-593,-117))
        click("test_game.png")
        click(Pattern("game_selection.png").targetOffset(547,323))
        wait("game_details.png")
        click(Pattern("game_details.png").targetOffset(-570,-258))
        wait(Pattern("video_clips.png").similar(0.80))
        click(Pattern("video_clips.png").similar(0.80).targetOffset(148,147))
        wait("save_as.png", 3)
        assert exists("save_as.png")
        
    def test02(self):
        type("DELETE ME")
        click(Pattern("save_as.png").targetOffset(311,38))
        click(Pattern("video_clips.png").similar(0.80).targetOffset(300,-144))
        wait(1)
        click("home.png")
        wait(2)
        click("manage_vsession.png")
        wait("vsession_manager.png")
        assert exists(Pattern("DELETE_ME.png").similar(0.80))
        
        rightClick(Pattern("DELETE_ME.png").similar(0.80))
        wait(Pattern("right_click_vsession.png"))
        click(Pattern("right_click_vsession.png").targetOffset(-43,18))
        wait(2)
        type(Key.ENTER)