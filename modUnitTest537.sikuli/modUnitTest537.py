from sikuli import *
import unittest
import shutil
import os
import util
gamelens = util.gamelens

def assignToTimeline():
    wait("right_click_menu.png")
    click(Pattern("right_click_menu.png").targetOffset(-52,-123))
    wait(1)

def buttonBlue():
    click(Pattern("blackboard_template_big_button.png").targetOffset(121,195))
    wait("color_selection.png")
    click(Pattern("color_selection.png").targetOffset(-61,-227))
    click(Pattern("color_selection.png").targetOffset(41,223))
    wait(1)

def textGreen():
    click(Pattern("blackboard_template_big_button.png").targetOffset(190,198))
    wait("color_selection.png")
    click(Pattern("color_selection.png").targetOffset(-60,120))
    click(Pattern("color_selection.png").targetOffset(41,223))
    wait(1)
    
def assignShortcut(char):
    click(Pattern("blackboard_big_button2.png").targetOffset(209,231))
    type(char)

def resizeText(fontSize):
    click(Pattern("blackboard_big_button2.png").targetOffset(256,198))
    type(Key.BACKSPACE + Key.BACKSPACE + fontSize + Key.ENTER)
    
class UnitTest537(unittest.TestCase):
    def test00setup(self):
        util.setUp()
    def testTearDown(self):
        util.tearDown()
        
    def test01(self):
        click(Pattern("advanced_menu.png").targetOffset(-581,-264))      
        wait(1)
        click(Pattern("advanced_menu.png").targetOffset(-431,84))
        wait("create_iga_menu.png")
        click(Pattern("create_iga_menu.png").targetOffset(294,-117))
        wait("blank_blackboard.png")
        click(Pattern("blank_blackboard.png").targetOffset(54,209))
        wait("blackboard_eop_button.png")
        assert exists("blackboard_eop_button.png")
        
    def test02(self):
        corner = find(Pattern("blackboard_eop_button.png").targetOffset(-175,-233))
        drop_point = corner.getTarget().offset(100, 100)
        dragDrop(corner, drop_point)
        assert exists("blackboard_template_big_button.png")
        wait(1)
        
    def test03(self):
        dragDrop(Pattern("blackboard_template_big_button.png").targetOffset(-190,-246), Pattern("blackboard_template_big_button.png").targetOffset(157,-253))
        wait(1)
        assert exists(Pattern("big_button_moved.png").similar(0.54))
        
    def test04(self):
        dragDrop(Pattern("blackboard_template_big_button.png").targetOffset(-240,-16), Pattern("blackboard_template_big_button.png").targetOffset(-70,73))
        wait(1)
        assert exists("blackboard_test_task.png")
        
    def test05(self):
        type("custom text" + Key.ENTER)
        assert exists(Pattern("custom_text_red.png").similar(0.89))
        
    def test06(self):
        buttonBlue()
        assert exists(Pattern("custom_text_blue_button.png").similar(0.85))   
        
    def test07(self):
        textGreen()
        assert exists(Pattern("custon_text_green.png").similar(0.85))
    
    def test08(self):
        resizeText("32")
        assert exists(Pattern("custom_text_green2.png").similar(0.86))
        
    def test09(self):
        assignShortcut("A")
        assert exists("custom_text_shortcut.png")
        
    def test10(self):
        click(Pattern("custom_text_white.png").targetOffset(74,-36))
        wait("custom_text_add_attr.png")
        assert exists(Pattern("custom_text_with_attr.png").similar(0.90))
        
    def test11(self):
        type("test" + Key.ENTER)
        buttonBlue()
        textGreen()
        resizeText("32")
        assignShortcut("B")
        
    def test12(self):
        click(Pattern("custom_text_add_attr.png").targetOffset(77,12))
        wait(1)
        
    def test13(self):
        click(Pattern("custom_text_green.png").targetOffset(76,-35))
        wait("custom_text_second_button.png")
        click(Pattern("custom_text_second_button.png").targetOffset(73,27))
        wait("custom_text_third_button.png")
        type("123" + Key.ENTER)
        buttonBlue()
        textGreen()
        resizeText("8")
        assignShortcut("C")
        assert exists(Pattern("third_button.png").similar(0.90))
        
    def test14(self):
        click(Pattern("blackboard_template_big_button.png").targetOffset(-213,194))
        type("test timeline" + Key.ENTER)
        assert exists(Pattern("timelines.png").similar(0.90).targetOffset(98,-27))
        
    def test15(self):
        click(Pattern("timelines.png").targetOffset(98,-27))
        assert not exists("test_timeline.png")
        
    def test16(self):
        click(Pattern("blackboard_template_big_button.png").targetOffset(-213,194))
        type("test timeline" + Key.ENTER)
        rightClick("custom_text_blue_a.png")
        assignToTimeline()
        assert exists(Pattern("custom_text_final.png").similar(0.90))
        
    def test17(self):
        click(Pattern("blackboard_template_big_button.png").targetOffset(-151,-351))
        buttonBlue()
        textGreen()
        resizeText("8")
        assignShortcut("D")
        assert exists(Pattern("custom_team_button.png").similar(0.90))
        
    def test18(self):
        rightClick("custom_text_blue_a.png")
        wait("right_click_menu.png")
        click(Pattern("right_click_menu.png").targetOffset(-53,144))
        wait(1)
        type("123" + Key.ENTER)
        assert exists(Pattern("custom_text_tasks.png").similar(0.90))
        
    def test19(self):
        dragDrop(Pattern("blackboard_big_button3.png").targetOffset(39,-101), Pattern("blackboard_big_button3.png").targetOffset(216,-48))
        click(Pattern("test_task.png").targetOffset(77,-19))
        click(Pattern("test_task_attribute.png").targetOffset(78,-44))
        click(Pattern("test_task_2_attributes.png").targetOffset(76,-14))
        click(Pattern("test_task_nested_attribute.png").targetOffset(11,-14))
        assert exists(Pattern("test_task_two_nested_attr.png").similar(0.90))

    def test19b(self):
        rightClick(Pattern("task_tree_before.png").targetOffset(71,16))
        wait("right_click_menu2.png")
        click(Pattern("right_click_menu2.png").targetOffset(-10,78))
        wait("confirm_attributes.png")
        click(Pattern("confirm_attributes.png").targetOffset(-37,22))
        wait(1)

    def test20(self):
        dragDrop(Pattern("blackboard_test_task_tree.png").targetOffset(83,31), Pattern("blackboard_test_task_tree.png").targetOffset(-179,-196))
        wait(1)
        assert exists(Pattern("test_task_moved.png").similar(0.90))

    def test21(self):
        rightClick(Pattern("test_task_tree.png").targetOffset(1,-54))
        wait("right_click_menu2.png")
        click(Pattern("right_click_menu2.png").targetOffset(-74,180))
        wait(1)
        assert exists(Pattern("blackboard_final2.png").similar(0.90))

    def test22(self):
        rightClick(Pattern("blackboard_final.png").targetOffset(154,-133))
        assignToTimeline()
        rightClick(Pattern("blackboard_final.png").targetOffset(-109,141))
        assignToTimeline()
        click(Pattern("blackboard_template_big_button.png").targetOffset(199,307))
        wait("template_save.png")
        type("123 Test Template")
        assert exists("template_save_screen.png")

    def test22a(self):
        click(Pattern("template_save.png").targetOffset(114,152))
        wait(2)
        if exists(Pattern("template_overwrite.png").similar(0.91)):
            click(Pattern("template_overwrite.png").targetOffset(-55,26))
        elif exists("template_exists_warning.png"):
            click(Pattern("template_exists_warning.png").targetOffset(35,31))
            wait("template_save.png")
            click(Pattern("template_save.png").targetOffset(-11,-133))
            type("2")
            click(Pattern("template_save.png").targetOffset(114,152))
            wait(2)
            if exists("template_overwrite.png"):
                click(Pattern("template_overwrite.png").targetOffset(-55,26))           
        wait(2) 
        click(Pattern("blackboard_template_big_button.png").targetOffset(216,345))
        wait("confirm_close.png")
        click(Pattern("confirm_close.png").targetOffset(-51,27))
        wait("create_iga_menu.png")
        click(Pattern("create_iga_menu.png").targetOffset(269,-112))
        wait("blank_blackboard.png")
        click(Pattern("blank_blackboard.png").targetOffset(212,290))
        wait("load_blackboard_template.png")
        assert exists("template_selection.png")

    def test23(self):
        if find(Pattern("test_template_name.png").similar(0.97)):
            click(Pattern("test_template_name.png").similar(0.97))
        else:
            click(Pattern("load_blackboard_template.png").targetOffset(-258,-77))
        click(Pattern("load_blackboard_template.png").targetOffset(120,155))
        wait(Pattern("blackboard_final.png").targetOffset(-109,141))
        assert exists(Pattern("blackboard_final.png").targetOffset(-109,141))
       
