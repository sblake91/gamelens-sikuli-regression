from sikuli import *
#all the paths
gamelens = r"C:\STATS\GameLensRU\GameLens"
folderPath = r"C:\Users\sblake\Desktop\GameLensRegression"
videoFolderPath= folderPath + r"\GLTest_Videos"
videoPath = videoFolderPath + r"\_170101_HOME_AWAY_V01_H1.mp4"
testVideoPath = videoFolderPath + r"\test_video.mp4"
screenshotsFolder = folderPath + r"\screenshots"
backgroundPath =  folderPath + r"\STATSbiglogo.jpg"
logoPath =  folderPath + r"\STATSlogo.png"
GLXPath = folderPath + r"\RU_00000123_Home Team_Away Team.glx"


def setUp():
    App.focus(gamelens)
    #wait(Pattern("1512403824910.png").exact(), 30)
    #click(Pattern("1512403824910.png").exact().targetOffset(28,38))
    wait(5)
    if exists("menu_select.png"):
        click("menu_select.png")
        wait(2)

def tearDown():
    App.close("gamelens")

def returnImageCount(image):
    count = 0
    for i in findAll(image):
        count += 1
    return count