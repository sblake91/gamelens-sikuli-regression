from sikuli import *
import shutil
import os
import util
import unittest

class UnitTest1308(unittest.TestCase):
    
    def test00setup(self):
        util.setUp()
        
    def testTearDown(self):
        rightClick("drawing_in_forum-1.png")
        wait("drawing_right_click_menu.png")
        click(Pattern("drawing_right_click_menu.png").targetOffset(-39,26))
        wait("confirm_delete_drawing.png")
        click(Pattern("confirm_delete_drawing.png").targetOffset(-50,32))
        wait(2)
        util.tearDown()

    def test01(self):
        click("manage_vsession.png")
        wait("vession_manager.png")
        if not exists(Pattern("test_vsession_expanded.png").similar(0.90)):
                click(Pattern("expand_test_vsession.png").similar(0.90).targetOffset(-38,1))
        rightClick(Pattern("expand_test_vsession.png").similar(0.90).targetOffset(3,2))
        wait("vsession_right_click1.png")
        click(Pattern("vsession_right_click1.png").targetOffset(-47,-109))
        click(Pattern("vession_manager.png").targetOffset(-455,258))
        click(Pattern("vession_manager.png").targetOffset(-275,383))
        wait("drawing_tools.png")
        click(Pattern("drawing_tools.png").targetOffset(-97,15))
        wait("drawing_font_size.png")
        click(Pattern("drawing_font_size.png").targetOffset(-6,30))
        click(Pattern("drawing_tools.png").targetOffset(-114,29))
        click(Pattern("vession_manager.png").targetOffset(-300,58))
        type("DRAWING")
        click(Pattern("drawing_tools.png").targetOffset(60,26))
        rightClick(Pattern("drawing_in_forum.png").similar(0.90))
        wait("drawing_right_click_menu.png")
        click(Pattern("drawing_right_click_menu.png").targetOffset(-45,0))
        wait("edit_drawing_menu.png")
        click(Pattern("edit_drawing_menu.png").targetOffset(97,-44))
        type(Key.BACKSPACE)
        paste("1000")
        click(Pattern("edit_drawing_menu.png").targetOffset(90,110))
        wait(1)
        assert exists(Pattern("1000_second_drawing.png").similar(0.90)) or exists(Pattern("1000_second_drawing_red.png").similar(0.90))
        

    def test02(self):
        rightClick("drawing_in_forum.png")
        wait("drawing_right_click_menu.png")
        click(Pattern("drawing_right_click_menu.png").targetOffset(-45,0))
        wait("edit_drawing_menu.png")
        click(Pattern("edit_drawing_menu.png").targetOffset(127,-43))
        type(Key.BACKSPACE + "1")
        click(Pattern("edit_drawing_menu.png").targetOffset(93,113))
        wait("display_length_safeties.png")
        assert exists("display_length_safeties.png")
        click(Pattern("display_length_safeties.png").targetOffset(-4,34))
        click(Pattern("edit_drawing_menu.png").targetOffset(186,111))
        
                