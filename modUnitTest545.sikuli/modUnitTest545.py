from sikuli import *
import unittest
import shutil
import os
import util
gamelens = util.gamelens
    
class UnitTest545(unittest.TestCase):
    
    def test00setup(self):
        util.setUp()
        
    def testTearDown(self):
        util.tearDown()
        
    def test2(self):
        if exists(Pattern("basic_home.png").similar(0.90)):
            click(Pattern("detailed_menu_button.png").similar(0.90))
            
            wait(2)
        click(Pattern("edit_game_template.png").exact())
        wait("game_selection.png")
        click(Pattern("game_selection.png").targetOffset(-603,-121))
        click(Pattern("game_selection.png").targetOffset(318,-292))
        click(Pattern("game_selection.png").targetOffset(293,-309))
        assert exists(Pattern("game_to_load.png").similar(0.80))
        
    def test2a(self):
        click(Pattern("game_selection.png").targetOffset(551,320))
        wait("game_info.png", 15)
        click(Pattern("game_info.png").targetOffset(-337,33))
        wait(1)
        assert exists(Pattern("advanced_game_info.png").similar(0.89).targetOffset(-270,-204))
        
    def test3(self):
        click(Pattern("advanced_game_info.png").targetOffset(-270,-204))
        type(Key.BACKSPACE + Key.BACKSPACE + "edited" + Key.ENTER)
        click(Pattern("advanced_game_info.png").targetOffset(-280,-181))
        type("(edited)" + Key.ENTER)
        click(Pattern("advanced_game_info.png").targetOffset(-255,-151))
        type("(edited)" + Key.ENTER)
        click(Pattern("advanced_game_info.png").targetOffset(-265,-123))
        type("(edited)" + Key.ENTER)
        click(Pattern("advanced_game_info.png").targetOffset(-432,-39))
        type("(edited)" + Key.ENTER)
        click(Pattern("advanced_game_info.png").targetOffset(-387,71))
        wait(1) 
        type(Key.BACKSPACE + Key.BACKSPACE + Key.BACKSPACE + Key.BACKSPACE)
        type("Rain")
        click(Pattern("advanced_game_info.png").targetOffset(-467,182))
        click(Pattern("advanced_game_info.png").targetOffset(-298,178))
        click(Pattern("advanced_game_info.png").targetOffset(-476,211))
        click(Pattern("advanced_game_info.png").targetOffset(-304,209))
        wait(1)
        assert exists(Pattern("advanced_game_info2.png").exact())
        
    def test4(self):
        click(Pattern("advanced_game_info.png").targetOffset(572,346))
        if exists("select_button.png", 10):
            click("select_button.png")
            wait(2)
        assert exists(Pattern("task_screen.png").similar(0.90))

    def test4a(self):
        click(Pattern("task_screen.png").targetOffset(-339,-303))
        wait(1)
        assert exists(Pattern("new_task.png").similar(0.90))        
        
    def test4b(self):
        click(Pattern("task_screen.png").targetOffset(318,249))
        wait("confirm_delete_new_tasks.png")
        click(Pattern("confirm_delete_new_tasks.png").targetOffset(-53,31))
        wait(1)
        assert exists(Pattern("task_screen.png").exact())