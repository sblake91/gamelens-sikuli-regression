from sikuli import *
import shutil
import os
import util
import unittest
gamelens = util.gamelens

class UnitTest1183(unittest.TestCase):
    def test00setup(self):
        util.setUp()
        
    def testTearDown(self):
        util.tearDown()

    def test01(self):
        click("manage_vsession.png")
        wait("vsession_view.png")
        try:
            find("video_tag.png")
        except FindFailed:
            click(Pattern("expand_test.png").targetOffset(-26,1))
            wait(1)
        rightClick("video_tag.png")
        wait(2)
        click(Pattern("1513718748484-1.png").targetOffset(-16,6))
        wait(2)
        if exists("overwrite_prompt.png"):
            click(Pattern("overwrite_prompt.png").targetOffset(45,30))
        click("workspace_button.png")
        wait(1)
        assert exists("exported_tag.png")
        rightClick("exported_tag.png")
        click(Pattern("delete_menu.png").targetOffset(-39,26))
        wait(2)
        wait("1513701044658.png")
        click(Pattern("1513701044658.png").targetOffset(-47,33))