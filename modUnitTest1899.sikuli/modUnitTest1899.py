from sikuli import *
import shutil
import os
import util
import unittest

class UnitTest1899(unittest.TestCase):
    
    def test00setup(self):
        util.setUp()
        
    def testTearDown(self):
        util.tearDown()

    def test01(self):
        click("manage_vsession.png")
        wait("vsession_viewer.png")
        if exists("test_vsession_expanded.png"):
            click(Pattern("vsession_test_collapse.png").targetOffset(-40,2))
        if exists("folder_expanded.png"):
            click(Pattern("folder_expanded.png").targetOffset(-182,-30))
        elif not exists("away_test_expanded.png"):
            click(Pattern("away_test_expand.png").similar(0.80).targetOffset(-49,-2))
        assert exists(Pattern("vsession_layout.png").similar(0.80))
        click(Pattern("vsession_viewer.png").targetOffset(547,-369))
        wait("confirm_layout_saved.png")
        click(Pattern("confirm_layout_saved.png").targetOffset(-5,29))
                
    def test02(self):
        click(Pattern("close_video_sessions_window.png").targetOffset(189,-45))
        click(Pattern("close_gamelens.png").similar(0.94).targetOffset(179,-96))
        
        wait("confirm_exit.png")
        click(Pattern("confirm_exit.png").targetOffset(-42,33))
        util.setUp()
        click("manage_vsession.png")
        wait("vsession_viewer.png")
        assert exists(Pattern("vsession_layout.png").similar(0.80))
             