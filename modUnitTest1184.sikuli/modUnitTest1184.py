from sikuli import *
import shutil
import os
import util
import unittest
gamelens = util.gamelens

class UnitTest1184(unittest.TestCase):
    def test00setup(self):
        util.setUp()
        
    def testTearDown(self):
        util.tearDown()

    def test01(self):
        click("manage_vsession.png")
        wait("vsession_view.png")
        rightClick(Pattern("test_vsession.png").similar(0.80))
        assert exists("right_click_vsession.png")
        
    def test02(self):
        click(Pattern("right_click_vsession.png").targetOffset(-18,111))
        wait(1)
        assert exists("default_star.png")
        rightClick(Pattern("test_vsession.png").similar(0.80))
        click(Pattern("right_click_vsession.png").targetOffset(-18,111))

