from sikuli import *
import shutil
import os
import util
reload(util)
import unittest
gamelens = util.gamelens

class UnitTest1891(unittest.TestCase):
    def test00setup(self):
        util.setUp()
        
    def testTearDown(self):
        os.remove(r"C:\STATS\GameLensRU\Game Video\DELETE_ME.mp4")
        os.remove(r"C:\STATS\GameLensRU\Game Video\DELETE_ME_TOO.mp4")
        os.remove(util.videoFolderPath + r"\test_video_converted.mp4")
        assert not os.path.exists(r"C:\STATS\GameLensRU\Game Video\DELETE_ME.mp4") and not os.path.exists(r"C:\STATS\GameLensRU\Game Video\DELETE_ME_TOO.mp4") and not os.path.exists(util.videoFolderPath + r"\test_video_converted.mp4")
        util.tearDown()

    def test01(self):
        click("manage_video.png")
        wait("video_importer.png")
        click(Pattern("video_importer.png").targetOffset(-291,-277))
        wait(1)
        paste(util.testVideoPath)
        wait(1)
        click("open_button.png")
        wait(1)
        assert exists(Pattern("test_video_in_menu.png").similar(0.90))

    def test02(self):
        click(Pattern("test_video_in_menu.png").similar(0.90))
        click(Pattern("video_importer.png").targetOffset(-227,-291))
        wait(1)
        assert not exists(Pattern("test_video_in_menu.png").similar(0.90))
        
    def test03(self):
        click(Pattern("video_importer.png").targetOffset(-291,-277))
        wait("folder_browse_field.png")
        click(Pattern("folder_browse_field.png").targetOffset(121,-1))
        type(util.videoFolderPath + Key.ENTER)
        wait("test_videos.png")
        dragDrop(Pattern("test_videos.png").targetOffset(171,-11), Pattern("test_videos.png").targetOffset(-169,0))
        click("open_button.png")
        wait(1)
        assert exists(Pattern("test_videos_in_menu.png").similar(0.75))

    def test04(self):
        click(Pattern("video_importer.png").targetOffset(-166,-286))
        assert exists(Pattern("video_importer.png").similar(0.97).targetOffset(-166,-286))

    def test05(self):
        click(Pattern("video_importer.png").targetOffset(-291,-277))
        wait(1)
        paste(util.testVideoPath)
        wait(1)
        click("open_button.png")
        wait(1)
        click(Pattern("video_importer.png").targetOffset(-100,-282))
        wait(Pattern("video_imported.png").exact(), 30)
        assert exists(Pattern("video_imported.png").exact()) and os.path.exists(util.videoFolderPath + r"\test_video_converted.mp4")

    def test06(self):
        click(Pattern("video_importer.png").targetOffset(-166,-286))
        click(Pattern("video_importer.png").targetOffset(-291,-277))
        wait(1)
        paste(util.testVideoPath)
        wait(1)
        click("open_button.png")
        wait(1)
        click(Pattern("video_importer.png").targetOffset(-44,-280))
        wait(Pattern("import_as.png").similar(0.90))
        paste("DELETE_ME")
        click(Pattern("import_as.png").similar(0.90).targetOffset(64,40))
        wait(Pattern("video_imported_as.png").exact(), 30)
        assert exists(Pattern("video_imported_as.png").exact()) and os.path.exists(r"C:\STATS\GameLensRU\Game Video\DELETE_ME.mp4")

    def test07(self):
        click(Pattern("video_importer.png").targetOffset(-166,-286))
        click(Pattern("video_importer.png").targetOffset(-291,-277))
        wait("folder_browse_field.png")
        click(Pattern("folder_browse_field.png").targetOffset(121,-1))
        type(util.videoFolderPath + Key.ENTER)
        wait("1513884308610.png")
        dragDrop(Pattern("1513884308610.png").targetOffset(-128,-4), Pattern("1513884308610.png").targetOffset(115,2))
        click("open_button.png")
        wait(1)
        click(Pattern("video_importer.png").targetOffset(27,-287))
        wait(Pattern("import_as.png").similar(0.90))
        paste("DELETE_ME_TOO")
        click(Pattern("import_as.png").similar(0.90).targetOffset(64,40))
        wait("merge_success.png", 60)
        assert exists("merge_success.png") and os.path.exists(r"C:\STATS\GameLensRU\Game Video\DELETE_ME_TOO.mp4")
        click(Pattern("merge_success.png").targetOffset(-12,24))

    def test08(self):
        click(Pattern("video_importer.png").targetOffset(-166,-286))
        click(Pattern("video_importer.png").targetOffset(-291,-277))
        wait("folder_browse_field.png")
        click(Pattern("folder_browse_field.png").targetOffset(121,-1))
        type(util.videoFolderPath + Key.ENTER)
        wait("test_videos.png")
        click("test_videos.png")
        click("open_button.png")
        click(Pattern("video_importer.png").targetOffset(-113,-272))
        click(Pattern("video_importer.png").targetOffset(77,-280))
        hover(Pattern("video_importer.png").targetOffset(5,72))
        assert exists(Pattern("resume_button.png").exact())

    def test09(self):
        click(Pattern("resume_button.png").exact())
        hover(Pattern("video_importer.png").targetOffset(5,72))
        assert exists(Pattern("pause_button.png").similar(0.90))
        click(Pattern("pause_button.png").similar(0.90))
                
    def test10(self):
        click(Pattern("video_importer.png").targetOffset(141,-285))
        hover(Pattern("video_importer.png").targetOffset(5,72))
        assert exists(Pattern("importing_stopped.png").exact())

    def test11(self):
        click(Pattern("video_importer.png").targetOffset(213,-271))
        wait("settings_menu.png")
        assert exists("settings_menu.png")

    def test12(self):
        click(Pattern("settings_menu.png").targetOffset(-31,-111))
        wait(Pattern("video_codec_dropdown.png").exact().targetOffset(-2,5))
        click(Pattern("video_codec_dropdown.png").exact().targetOffset(1,8))
        click(Pattern("settings_menu.png").targetOffset(-25,-64))
        wait(Pattern("resolution_select.png").exact())
        click(Pattern("resolution_select.png").exact().targetOffset(-77,-18))
        click(Pattern("settings_menu.png").targetOffset(-115,66))
        click(Pattern("settings_menu.png").targetOffset(90,163))
        assert exists(Pattern("customised_settings.png").exact())

    def test13(self):
        click(Pattern("video_importer.png").targetOffset(213,-271))
        click(Pattern("settings_menu.png").targetOffset(-31,-111))
        wait(Pattern("video_codec_dropdown2.png").exact())
        click(Pattern("video_codec_dropdown2.png").exact().targetOffset(-5,-12))
        click(Pattern("settings_menu.png").targetOffset(-25,-64))
        wait(Pattern("resolution_dropdown2.png").exact().targetOffset(-52,22))
        click(Pattern("resolution_dropdown2.png").exact().targetOffset(-52,22))
        click(Pattern("settings_menu.png").targetOffset(-115,66))
        click(Pattern("settings_menu.png").targetOffset(90,163))
        assert exists(Pattern("1513888692491.png").exact())
        
        