from sikuli import *
import shutil
import os
import util
import unittest

class UnitTest549(unittest.TestCase):
    
    def test00setup(self):
        util.setUp()
        
    def testTearDown(self):
        util.tearDown()

    def test01(self):
        click("1514310454337.png")
        wait(1)
        click("all_reports_button.png")
        if not exists("test_matrix_report.png"):
            click("1518623236168.png")
            wait(1)
            type('Test Matrix Report.pzjson' + Key.ENTER)
        wait(Pattern("test_matrix_report.png").similar(0.90))
        assert exists(Pattern("test_matrix_report.png").similar(0.90))

    def test02(self):
        click(Pattern("test_matrix_report.png").similar(0.80))
        doubleClick("competition_filter.png")
        assert exists(Pattern("specified_filtering.png").exact())

    def test03(self):
        click(Pattern("specified_filtering.png").exact().targetOffset(5,140))
        assert exists(Pattern("test_report_with_filter.png").exact())

    def test04(self):
        click(Pattern("test_report_with_filter.png").exact().targetOffset(-108,14))
        click(Pattern("specified_filtering.png").targetOffset(87,143))
        wait("load_game_screen.png")
        click(Pattern("test_competition.png").similar(0.90))
        click("test_game.png")
        click(Pattern("load_game_screen.png").targetOffset(545,332))
        wait("test_game_report.png")
        assert exists("test_game_report.png")

    def test05(test):
        click(Pattern("close_button.png").similar(0.90))
        click("home_button.png")
        click("review_a_game.png")
        wait("load_game_screen.png")
        click(Pattern("test_competition.png").similar(0.90))
        click("test_game.png")
        click(Pattern("load_game_screen.png").targetOffset(539,325))
        wait(1)
        click(Pattern("reporting_button.png").similar(0.90))
        click("all_reports_button.png")
        wait("test_matrix_report.png")
        click(Pattern("test_report_with_filter.png").exact().targetOffset(-108,14))
        click(Pattern("specified_filtering.png").targetOffset(87,143))
        wait("test_game_report.png")
        assert exists("test_game_report.png")

    def test06(self):
        click("test_game_report.png")
        click(Pattern("close_button.png").similar(0.90))
        wait(2)
        #click(Pattern("test_report_with_filter.png").exact().targetOffset(-108,14))
        click(Pattern("specified_filtering.png").targetOffset(-69,-122))
        click(Pattern("specified_filtering.png").targetOffset(68,-144))
        click(Pattern("specified_filtering.png").targetOffset(5,140))