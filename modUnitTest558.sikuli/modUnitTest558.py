from sikuli import *
import shutil
import os
import util
import unittest
gamelens = util.gamelens

class UnitTest558(unittest.TestCase):
    def test00setup(self):
        util.setUp()
        
    def testTearDown(self):
        util.tearDown()

    def test01(self):
        click("manage_vsession.png")
        wait("vsession_view.png", 10)
        rightClick("test2.png")
        assert exists("right_click.png")
        click("vsession_view.png", 10) 

    def test02(self):
        if exists("vfile.png"):
            rightClick("vfile.png")
        else:
            click(Pattern("1514305690045.png").targetOffset(-14,1))
            wait(1)
            rightClick("vfile.png")
        assert exists("right_click_tag.png")