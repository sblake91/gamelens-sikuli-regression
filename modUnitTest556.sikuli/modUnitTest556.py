from sikuli import *
import shutil
import os
import util
import unittest
gamelens = util.gamelens

class UnitTest556(unittest.TestCase):
    def test00setup(self):
        util.setUp()
        
    def testTearDown(self):
        util.tearDown()

    def test01(self):
        click("task_filter_engine.png")
        wait(Pattern("task_filter_engine_screen.png").targetOffset(-609,328),5)
        click(Pattern("red_plus.png").similar(0.90))
        
        wait("new_favourite.png", 3)
        click(Pattern("new_favourite.png").similar(0.80).targetOffset(59,44))
        wait(1)
        assert exists("favourite_6.png")

    def test02(self):
        dragDrop(Pattern("favorite_6_and_trash.png").targetOffset(-101,-192), Pattern("favorite_6_and_trash.png").targetOffset(120,182))
        
        wait("confirm_delete_selected_item.png", 5)
        click(Pattern("confirm_delete_selected_item.png").targetOffset(-51,33))
        wait(3)
        assert not exists(Pattern("favourite_6.png").exact())
        
        