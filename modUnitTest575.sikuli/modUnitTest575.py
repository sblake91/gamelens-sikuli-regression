from sikuli import *
import shutil
import os
import util
import unittest

class UnitTest575(unittest.TestCase):
    
    def test00setup(self):
        util.setUp()
        
    def testTearDown(self):
        util.tearDown()

    def test01(self):
        click("manage_vsession.png")
        wait("vsession_list.png")
        rightClick("test_vsession.png")
        wait(Pattern("rightclick_vsession.png").targetOffset(-46,78))
        click(Pattern("rightclick_vsession.png").targetOffset(-46,78))
        wait("save_video_and_xml_prompt.png")
        click(Pattern("save_video_and_xml_prompt.png").targetOffset(41,28))
        wait(2)
        paste("DELETE_ME")
        type(Key.ENTER)
        wait("1514387436160.png", 120)
        click(Pattern("1514387436160.png").targetOffset(-10,32))
        assert os.path.exists(r"C:\STATS\GameLensRU\Edited Clips\DELETE_ME.mp4") and os.path.exists(r"C:\STATS\GameLensRU\Edited Clips\XML-DELETE_ME.xml")

    def test02(self):
        os.remove(r"C:\STATS\GameLensRU\Edited Clips\DELETE_ME.mp4")       
        os.remove(r"C:\STATS\GameLensRU\Edited Clips\XML-DELETE_ME.xml")
        assert not os.path.exists(r"C:\STATS\GameLensRU\Edited Clips\DELETE_ME.mp4") and not os.path.exists(r"C:\STATS\GameLensRU\Edited Clips\XML-DELETE_ME.xml")
        