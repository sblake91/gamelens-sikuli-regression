from sikuli import *
import shutil
import os
import util
import unittest

class UnitTest1532(unittest.TestCase):
    
    def test00setup(self):
        util.setUp()
        
    def testTearDown(self):
        util.tearDown()

    def test01(self):
        click("manage_vsession.png")
        if exists("test_vsession_expanded.png"):
            click(Pattern("vsession_test_collapse.png").targetOffset(-40,2))
        if exists("folder_expanded.png"):
            click(Pattern("folder_expanded.png").targetOffset(-182,-30))
        elif not exists("away_test_expanded.png"):
            click(Pattern("away_test_expand.png").similar(0.80).targetOffset(-49,-2))
        hover(Pattern("away_test_expanded.png").targetOffset(-109,29))
        mouseDown(Button.LEFT)
        hover("vsession_test_collapse.png")
        wait(3)
        hover(Pattern("away_test_expanded.png").targetOffset(-109,29))
        mouseUp(Button.RIGHT)
        assert exists(Pattern("test_vsession_expanded.png").similar(0.90))

    def test02(self):
        hover(Pattern("away_test_expanded.png").targetOffset(-109,29))
        mouseDown(Button.LEFT)
        hover(Pattern("away_test_expanded.png").targetOffset(-147,-12))
        wait(3)
        hover(Pattern("folder_expanded.png").targetOffset(-125,49))
        mouseUp(Button.RIGHT)
        assert exists(Pattern("folder_expanded.png").similar(0.90))
        
            
        