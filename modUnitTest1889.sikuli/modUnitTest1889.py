from sikuli import *
import shutil
import os
import util
import unittest

class UnitTest1889(unittest.TestCase):
    def test00setup(self):
        util.setUp()
        
    def testTearDown(self):
        util.tearDown()

    def test01(self):
        click("manage_video.png")
        wait("select_video_folder.png")
        click(Pattern("select_video_folder.png").targetOffset(121,-3))
        wait(1)
        type(util.videoFolderPath)
        click("select_folder.png")
        wait(1)
        assert exists("video_list.png")
        
    def test02(self):
        dragDrop(Pattern("column_resize.png").targetOffset(79,-37), Pattern("column_resize.png").targetOffset(-292,-42))
        assert exists("video_names_hidden.png")

    def test03(self):
        dragDrop(Pattern("video_names_hidden.png").targetOffset(-250,-38), Pattern("video_names_hidden.png").targetOffset(111,-36))
        
        click("test_video_to_rename.png")
        click(Pattern("rename_to_user_defined.png").similar(0.90))
        wait(Pattern("rename_prompt.png").similar(0.90))
        type("hello_")
        click(Pattern("rename_prompt.png").similar(0.85).targetOffset(58,45))
        
        wait(2)
        assert exists(Pattern("1513791511211.png").similar(0.90))
        
    def test04(self):
        click(Pattern("search_for_game_video.png").similar(0.90))
        wait("game_find_screen.png")
        click(Pattern("game_find_screen.png").targetOffset(-596,-123))
        click("game_to_check.png")
        click(Pattern("game_find_screen.png").targetOffset(562,321))
        wait("game_video_list.png")
        assert exists(Pattern("game_video_list.png").similar(0.80).targetOffset(21,234))
        click(Pattern("game_video_list.png").similar(0.80).targetOffset(21,234))
        wait(1)
        
    def test05(self):
        click(Pattern("load_game_info.png").targetOffset(-3,-37))
        
        wait("game_find_screen.png")
        click(Pattern("game_find_screen.png").targetOffset(-596,-123))
        click("game_to_check.png")
        click(Pattern("game_find_screen.png").targetOffset(562,321))
        wait("renaming_convention.png")
        click(Pattern("renaming_convention.png").targetOffset(-123,-2))
        wait(0.5)
        click(Pattern("third_view.png").targetOffset(-56,-12))
        click(Pattern("renaming_convention.png").targetOffset(192,64))
        wait(2)
        if exists(Pattern("video_overwrite_prompt.png").targetOffset(-91,29)):
            click(Pattern("video_overwrite_prompt.png").targetOffset(-91,29))
        wait(1)
        assert exists(Pattern("renamed_video_2.png").similar(0.90))

    def test06(self):
        click(Pattern("search_for_game_video.png").similar(0.90))
        wait("game_find_screen.png")
        click(Pattern("game_find_screen.png").targetOffset(-596,-123))
        click("game_to_check.png")
        click(Pattern("game_find_screen.png").targetOffset(562,321))
        wait("game_video_list2.png")
        assert exists(Pattern("game_video_list2.png").similar(0.95))
        click(Pattern("game_video_list2.png").targetOffset(2,225))
        
    def test07(self):
        click(Pattern("rename_to_user_defined.png").similar(0.90))
        wait("change_name_back.png")
        dragDrop(Pattern("change_name_back.png").similar(0.90).targetOffset(102,-13), Pattern("change_name_back.png").similar(0.90).targetOffset(-218,-9))
        wait(1)
        type(Key.BACKSPACE)
        type("test_video.mp4")
        click(Pattern("change_name_back.png").targetOffset(60,35))
        assert exists("video_changed_back.mp4.png")
        