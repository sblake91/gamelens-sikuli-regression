from sikuli import *
import shutil
import os
import util
import unittest

class UnitTest1898(unittest.TestCase):
    
    def test00setup(self):
        util.setUp()
        
    def testTearDown(self):
        util.tearDown()

    def test01(self):
        click("manage_vsession.png")
        wait("vession_manager.png")
        if not exists(Pattern("test_vsession_expanded.png").similar(0.90)):
                click(Pattern("expand_test_vsession.png").similar(0.90).targetOffset(-38,1))
        doubleClick(Pattern("expand_test_vsession.png").similar(0.90).targetOffset(9,-1))
        rightClick(Pattern("vession_manager.png").targetOffset(3,279))
        wait(1)
        assert exists(Pattern("clip_list.png").similar(0.90))

    def test02(self):
        click(Pattern("clip_list.png").similar(0.90).targetOffset(-29,11))
        wait(1)
        assert exists("clip_3_of_4.png") and exists("clip_3_title.png")
        
        