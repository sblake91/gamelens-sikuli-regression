﻿# README #
### What is this repository for? ###

Sikuli automation for several tests in the GameLens Regression Suite. (Currently Windows Only)

### What is Sikuli? ###

[SikuliX](http://www.sikulix.com) automates anything you see on the screen of your desktop computer running Windows, Mac or some Linux/Unix. It uses image recognition powered by OpenCV to identify and control GUI components.

### How do I get set up? ###

* Open the GameLens.zip and follow the instructions in the readme to set up GameLens.
* [Install Sikuli](http://www.sikulix.com/quickstart/)
* Create folder 'C:\Live'
* In Windows display preferences make sure apps aren't scaled. Resolution 1920 x 1080. 
* I recommend changing your desktop background to a solid color. Preferably one that doesn't exist in GameLens. This is to minimize false matches in Sikuli.
* Copy '_170101_HOME_AWAY_V01_H1' and '_170101_HOME_AWAY_V02_H1' from 'GLTest_Videos' in this repository to 'C:\STATS\GameLensRU\Game Video'
* Copy 'exported_gamelens_db_08-01-2018_1455.glsql' from this repository to 'C:\GamesAdmin\DB Backups\Rollback Snapshots'
* Copy 'Test Matrix Report.pzjson' from the repository to 'C:\STATS\GameLensRU\ReportExports'
* Open GameLens
* Navigate to Advanced Management
* Import the 'exported_gamelens_db_08-01-2018_1455.glsql' database
* Close GameLens
* Run Sikulix
* In Sikuli: file > open > util.sikuli
* In util.sikuli change the folderPath to wherever you clone this repository.
* In Sikuli: file > open > GameLensUnitTest.sikuli
* Click run and let Sikuli do its thing.
* So far there are about 30 tests in this Suite that test various aspects of the GameLens UI
* The tests should take 15-30 minutes to run. You will be unable to use your mouse and keyboard.
* When the test have finished the App should close and the Sikuli IDE should reappear with a simple summary
* Navigate to the Report.html in this folder and open it up to see a more detailed summary of the test results
* Failing tests should provide an error message as well as a link to a screenshot

### Adding Tests ###

* This suite uses Sikuli script in a Python UnitTest framework.
* In Sikuli IDE: File > New
* The tests are numbered after corresponding tests on [Jira](https://statsinc.atlassian.net/plugins/servlet/ac/com.thed.zephyr.je/general-executions-enav?project.id=25205&project.key=GAMELENS#!view=list&offset=1&zql=project%20%3D%20%22GAMELENS%22%20AND%20fixVersion%20%3D%20%22Unscheduled%22%20AND%20cycleName%20%3D%20%22Regression%20Tests%22&viewMode=basic)
* Save As modUnitTest*YourTestNumber*
* Import the necessary dependencies:
```python
from sikuli import *
import shutil
import os
import util
import unittest

class UnitTest*YourTestNumber*(unittest.TestCase):
    def test00setup(self):
        util.setUp()
        
    def testTearDown(self):
        util.tearDown()
```
* UnitTest runs tests in Alphanumerical order. ex: test1 will run before test2 but test11 will also run before test2. Therefore it is best to pad test numbers with 0s. 
* In typical unit testing each test would be self-contained however in testing this complex app I've found it necessary for tests to run sequentially. Therefore this is more of an integration test suite but utilizing the unittest framework.
* Write your tests!
* General Tips:
    * Do some simple Sikuli tutorials first to get a feel for its capabilities.
    * Run through the regression test manually before attempting to write the test in Sikuli
    * Give your screen shots descriptive names! This will make debugging much easier.
    * Make the most of Sikuli images by using the matching preview and target offset menus
    * It can be useful to write the Sikuli script first before applying the unittest functionality and assertments
* Import your test to GameLensUnitTest.sikuli:
```python
from modUnitTest*YourTestNumber* import *

...
...

suite.addTests(unittest.TestLoader().loadTestsFromTestCase(UnitTest*YourTestNumber*))

```
### Completed Tests ###
* [GAMELENS-2: Show drawing as individual line in the vSession](https://statsinc.atlassian.net/browse/GAMELENS-2?jql=id%3DGAMELENS-2)
* [GAMELENS-4: Show drawing in video sessions](https://statsinc.atlassian.net/browse/GAMELENS-4?jql=id%3DGAMELENS-4)
* [GAMELENS-5: Highlight the item currently being played](https://statsinc.atlassian.net/browse/GAMELENS-5?jql=id%3DGAMELENS-5)
* [GAMELENS-537: Create Quick IGA blackboards](https://statsinc.atlassian.net/browse/GAMELENS-537?jql=id%3DGAMELENS-537)
* [GAMELENS-538: Personalize via preferences](https://statsinc.atlassian.net/browse/GAMELENS-538?jql=id%3DGAMELENS-538)
* [GAMELENS-539: Reviewing coded game](https://statsinc.atlassian.net/browse/GAMELENS-539?jql=id%3DGAMELENS-539)
* [GAMELENS-540: Importing client GLXs](https://statsinc.atlassian.net/browse/GAMELENS-540?jql=id%3DGAMELENS-540)
* [GAMELENS-541: Create client quick IGA game](https://statsinc.atlassian.net/browse/GAMELENS-541?jql=id%3DGAMELENS-541)
* [GAMELENS-543: Client should be able to manage all aspects of GameLens](https://statsinc.atlassian.net/browse/GAMELENS-543?jql=id%3DGAMELENS-543)
* [GAMELENS-545: Clients should be able to edit certain aspects to game setup](https://statsinc.atlassian.net/browse/GAMELENS-545?jql=id%3DGAMELENS-545)
* [GAMELENS-549: Loading Reports](https://statsinc.atlassian.net/browse/GAMELENS-549?jql=id%3DGAMELENS-549)
* [GAMELENS-554: Task filter engine- filtering](https://statsinc.atlassian.net/browse/GAMELENS-554?jql=id%3DGAMELENS-554)
* [GAMELENS-556: Task filter engine - Adding and removing categories](https://statsinc.atlassian.net/browse/GAMELENS-556?jql=id%3DGAMELENS-556)
* [GAMELENS-558: Vsession right click options](https://statsinc.atlassian.net/browse/GAMELENS-558?jql=id%3DGAMELENS-558)
* [GAMELENS-559: Toggle between 9 views](https://statsinc.atlassian.net/browse/GAMELENS-559?jql=id%3DGAMELENS-559)
* [GAMELENS-575: GamePlan Exports Test](https://statsinc.atlassian.net/browse/GAMELENS-575?jql=id%3DGAMELENS-575)
* [GAMELENS-1180: Allow user to edit a drawing](https://statsinc.atlassian.net/browse/GAMELENS-1180?jql=id%3DGAMELENS-1180)
* [GAMELENS-1181: Allow user to edit playback settings of a drawing](https://statsinc.atlassian.net/browse/GAMELENS-1181?jql=id%3DGAMELENS-1181)
* [GAMELENS-1183: Exporting tag in the new framework](https://statsinc.atlassian.net/browse/GAMELENS-1183?jql=id%3DGAMELENS-1183)
* [GAMELENS-1184: Allow user to set a folder to send exported tags or videos into](https://statsinc.atlassian.net/browse/GAMELENS-1184?jql=id%3DGAMELENS-1184)
* [GAMELENS-1308: Safeties needed for drawing times](https://statsinc.atlassian.net/browse/GAMELENS-1308?jql=id%3DGAMELENS-1308)
* [GAMELENS-1319: Implement new 'Export to Single Video' in Video Sessions](https://statsinc.atlassian.net/browse/GAMELENS-1319?jql=id%3DGAMELENS-1319)
* [GAMELENS-1532: Expand any vSession/Folders if a user drags an item on top of it](https://statsinc.atlassian.net/browse/GAMELENS-1532?jql=id%3DGAMELENS-1532)
* [GAMELENS-1535: Expand a collapsed vSession if new items are added to it](https://statsinc.atlassian.net/browse/GAMELENS-1535?jql=id%3DGAMELENS-1535)
* [GAMELENS-1886: Re-enable save as vSession button in 4.0](https://statsinc.atlassian.net/browse/GAMELENS-1886?jql=id%3DGAMELENS-1886)
* [GAMELENS-1889: Manage Video Screen](https://statsinc.atlassian.net/browse/GAMELENS-1889?jql=id%3DGAMELENS-1889)
* [GAMELENS-1891: Video Converter Screen](https://statsinc.atlassian.net/browse/GAMELENS-1889?jql=id%3DGAMELENS-1889)
* [GAMELENS-1898: Right click function to skip to clip number](https://statsinc.atlassian.net/browse/GAMELENS-1898?jql=id%3DGAMELENS-1898)
* [GAMELENS-1899: vSession setup to save to the database](https://statsinc.atlassian.net/browse/GAMELENS-1899?jql=id%3DGAMELENS-1899)

### Remaining Tests ###
Any [Regression Tests](https://statsinc.atlassian.net/plugins/servlet/ac/com.thed.zephyr.je/general-executions-enav?project.id=25205&project.key=GAMELENS#!view=list&offset=1&zql=project%20%3D%20%22GAMELENS%22%20AND%20fixVersion%20%3D%20%22Unscheduled%22%20AND%20cycleName%20%3D%20%22Regression%20Tests%22&viewMode=basic) that aren't in the above list I was not able to automate with SikuliX. For the most part, tests that involve video playback and coding IGA games may have to be tested manually for the time being.


### A few notes ###
* Test03 in UnitTest1180 fails because it is currently possible to undo saved drawings and [should not be.](https://statsinc.atlassian.net/browse/GAMELENS-1180?jql=id%3DGAMELENS-1180)
* A failing test does not necessarily mean a problem with the application as sometimes Sikuli acts unpredictably. It is best to check failing tests manually or to rerun them individually.
* In some instances an incomplete test may leave behind unwanted files or data that will have to be deleted in order for the tests to function properly. 

### Useful Links ###

* [Sikuli Forum](https://answers.launchpad.net/sikuli)
* [Sikuli Into Tutorial](https://www.youtube.com/watch?v=VdCOg1bCmGo)
* [HTMLRunner](https://answers.launchpad.net/sikuli/+question/176005)
* [UnitTest Documentation](https://docs.python.org/2/library/unittest.html)

### Who do I talk to? ###

spencer:
sblake@stats.com
