from sikuli import *
import shutil
import os
import util
import unittest
gamelens = util.gamelens

class UnitTest541(unittest.TestCase):
    def test00setup(self):
        util.setUp()
        
    def testTearDown(self):
        util.tearDown()
        
    def test01(self):
        click(Pattern("advanced_menu.png").targetOffset(-592,-261))
        
        click("create_quick_iga_button.png")
        wait(Pattern("create_iga_screen.png").targetOffset(-113,-310))
        assert exists(Pattern("create_iga_screen.png").similar(0.80).targetOffset(-113,-310))
        
    def test02(self):
        click(Pattern("create_iga_screen.png").targetOffset(-113,-310))
        type("test")
        click(Pattern("create_iga_screen.png").targetOffset(-88,-204))
        type("Home Team" + Key.ENTER)
        click(Pattern("create_iga_screen.png").targetOffset(-84,-161))
        type("Away Team" + Key.ENTER)
        click(Pattern("create_iga_screen.png").targetOffset(46,-24))
        wait(1)
        type("C:\Live")
        click("select_folder.png")
        wait(1)
        click(Pattern("create_iga_screen.png").targetOffset(348,106))
        wait("template_load_screen.png")
        click(Pattern("template_load_screen.png").targetOffset(-252,-80))
        click(Pattern("template_load_screen.png").targetOffset(120,150))
        wait(1)
        click(Pattern("create_iga_screen.png").targetOffset(-75,147))
        wait("coder_list.png", 30)
        assert exists("coder_list.png")
        
    def test03(self):
        click(Pattern("coder_list.png").targetOffset(253,182))
        click(Pattern("create_iga_screen.png").targetOffset(276,318))
        wait("created_success.png")
        assert exists(Pattern("created_success.png").similar(0.90))
        
    def test04(self):
        click(Pattern("created_success.png").targetOffset(-1,26))
        wait(2)
        click("master_coder_button.png")
        wait("iga_game_select.png")
        click(Pattern("iga_game_select.png").targetOffset(-611,-120))
        click(Pattern("iga_game_select.png").targetOffset(292,-305))
        click(Pattern("iga_game_select.png").targetOffset(539,318))
        wait(2)
        assert exists("coder_ui.png")
